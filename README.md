# Tuition Reimbursement Management System

## Project Description
The Tuition Reimbursement Management System is used for tracking, approving, and dispersing reimbursements for continued education. An employee can apply for an educational course, which their direct supervisor, department head, and benefits coordinator must approve.

## Technologies used
- Appian 20.4
- MySQL Database
- SQL
- JDBC
- Jira

## Features
- As the System:
    - I can route a reimbursement request through each department
    - I will send an automated email response as the request is processed
- As an Employee:
    - I can login to the application and submit or cancel a tuition reimbursement request
    - I can upload approval documents from higher ups to my reimbursement request
- As a Supervisor:
    - I can approve or reject reimbursement requests
    - I can message the Employee about a pending request
- As a Department Head:
    - I can review and manage requests approved by a Supervisor
    - I can approve or reject reimbursement requests
    - I can message the Employee or their Supervisor about a pending request
- As a Benefits Coordinator:
    - I have the final say in approving or rejecting a request
    - I can alter the reimbursement amount 
    - I can message the Employee, their Supervisor, or their Department Head about a pending request

## Getting Started
- Requires being created as a user for the Revature Appian Cloud development environment.
- Log into the Appian Cloud environment using credentials given by a system administrator: [Login Page](https://revaturedev.appiancloud.com/).
- Once logged in, users should be redirected to the following Appian site: T3 Reimbursement Site.

## Usage
Once logged in, business users will have access to the following site pages based on the security group they belong to:

- Employees, Direct Supervisors, Department Heads, and Benefits Coordinators:
    - My Requests: Allows you to submit a new reimbursement request, as well as view all requests that belong to you.
        - Further details about a reimbursement request can viewed by clicking the request's ID number within the list of records.
    - Active Tasks: Allows you to view and complete tasks assigned to you that pertain to the reimbursement approval process.
        - Possible tasks:
            - Approving/rejecting a request
            - Responding to a request for additional information
            - Approving/canceling a submitted request that was altered by the Benefits Coordinator
- Benefits Coordinators:
    - Reimbursement Report: Displays a pie chart that shows the number of reimbursement requests by status.
        - When a pie slice is selected, all reimbursement requests with that particular status are then displayed below the pie chart within a grid layout.
        - Further details about a reimbursement request can viewed by clicking the request's ID number within the grid.

## Contributors
- Project Team Members:
    - Jahdiel Evans
    - Jon Baker
    - Casey McCurley
    - Edgar Aragon
    - Bryan Mejia
    - Branden Aldridge
    - Stroh Leslie

## License
This project uses the following license: [MIT License](https://gitlab.com/210104-appian/jahdiel_evans/project-3-tuition-reimbursement-management-system/-/blob/master/LICENSE).
